# [Projet 01 (Complet) : CV en HTML / CSS](https://code-ouvert.gitlab.io/projet-01-complet-cv-en-html-css)

## À propos de Code Ouvert et du promoteur

**Code Ouvert** est un blog de codage où je publie des artlcles liés à HTML, CSS, JavaScript, PHP... ainsi que des framework tels que symfony, laravel... Ici, je fournis également gratuitement les codes sources de chacune de mes [vidéos YouTube](https://www.youtube.com/c/CodeOuvert) et vous pouvez utiliser ces codes sans aucune restriction ni limitation. Je crois que mes vidéos ou codes aident à inspirer les concepteurs et développeurs Web et aident également à améliorer leurs compétences.

Je m'appelle **Yassin El Kamal NGUESSU** et je suis un **développeur backend** d'abord autodidacte puis diplomé d'une licence en genie informatique. J'ai également travaillé sur de nombreux **projets front-end** dans le passé et j'y travaille toujours. Internet et le développement Web sont ma passion et je crois qu'il est important d'aider les gens avec mes capacités et mes connaissances. J'apprends ces choses depuis 2016 et j'ai l'impression que l'apprentissage fait désormais partie de ma vie.

## YouTube

[Lien de la video YouTube du projet](https://www.youtube.com/watch?v=hZ0ja7d3eHE&t=570s)

## Demo

[https://code-ouvert.gitlab.io/projet-01-complet-cv-en-html-css](https://code-ouvert.gitlab.io/projet-01-complet-cv-en-html-css)

## Objectifs

Qu’est ce que nous allons voir aujourd’hui? Dans ce nouveau projet, je vous propose de **créer une page complète en HTML et en CSS** qui sera une **page de CV**. Pour cela, nous allons utiliser les **notions de flexbox, le responsive design et les éléments HTML5 structurants.**

## Pré-requis

- Les base en HTML5 et CSS3

## Outils de développement

- Un ordinateur (la précision en vaut la peine)

- Un Editeur de text (Exemple : [Sublime Text](https://www.sublimetext.com/), [visual Studio Code](https://code.visualstudio.com/), ...)

- Trois Navigateurs ou plus pour les tests (Google Chrome, Mozilla Firefox
, Microsoft Edge, ...)

## Ressources

- https://www.youtube.com/watch?v=hZ0ja7d3eHE

## Création des dossiers et fichiers du projet

```
cv/
├── assets/
│   ├── css/
│   │   ├── style.css
│   │
│   ├── files/
│   │	├── cv.pdf
│   │   
│   ├── images/
│	├── default_user.jpg
│
├── index.html
│   
├── LICENSE
```

